#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int N, i, temp;
    cin >> N;
    int* numbers = new int[N];
    for (i = 0; i < N; i++) {
        cin >> temp;
        numbers[i] = temp;
    }
    for (i = N; i > 0; i--) printf("%d ", numbers[i - 1]);
    return 0;
}
