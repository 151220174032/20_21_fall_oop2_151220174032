#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    int varInt;
    long varLong;
    char varChar;
    float varFloat;
    double varDouble;

    cin >> varInt >> varLong >> varChar >> varFloat >> varDouble;
    printf("%d\n%ld\n%c\n%.3lf\n%.9lf", varInt, varLong, varChar, varFloat, varDouble);
    return 0;
}