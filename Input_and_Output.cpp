#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int temp = 0, sum = 0;
    while (cin >> temp) {
        sum += temp;
    }
    cout << sum;
    return 0;
}
