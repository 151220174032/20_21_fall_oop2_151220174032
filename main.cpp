/*!
 *  @file   main.cpp
 *  @brief  Some operations on the input that is read from a file
 *  @author Omer Faruk Sahin - 151220174032
 *  @date   2020-11-01
 */
#include<iostream>
#include<iomanip>
#include<fstream>
#include<string>
#include<sstream>

using namespace std;

/*!
 * Get numbers from a string (Convert strings to integers). 
 * Check the number of integers in the string whether it is equal to expected number.
 *
 * @param nums the string that contains integers.
 * @param numbers[] an array that is going to store the integers in the string.
 * @param numOfIntegers expected number of integers
 * @return if the number of integers in the string is equal to expected number = 1, if it is not = 0.
 */
int getNumbers(string nums, int numbers[], int numOfIntegers) {
	stringstream Nums(nums);
	string tempStr;
	int temp, j = 0;
	
	while (!Nums.eof() && j!=numOfIntegers+1) { // Condition breaks the loop if there are too many numbers
		Nums >> tempStr;
		if (stringstream(tempStr) >> temp) {
			numbers[j] = temp;
			j++;
		}
		tempStr = "";
	}		

	if (j != numOfIntegers) {
		cout << "Error : Wrong number of integers!"<<endl;
		return 0;
	}
	if (j == numOfIntegers) return 1;
}

/*!
 * Sum integers that are given in the file.
 *
 * @param arr[] the array that stores the integers.
 * @param numOfIntegers the number of integers.
 * @return sum of the integers.
 */
int findSum(int arr[], int numOfIntegers) { 
	int i, sum = 0;
	for (i = 0; i < numOfIntegers; i++) sum = sum + arr[i];
	return sum;
}

/*!
 * Multiplicate integers that are given in the file.
 *
 * @param arr[] the array that stores the integers.
 * @param numOfIntegers the number of integers.
 * @return prouct of the integers.
 */
int findProduct(int arr[], int numOfIntegers) { 
	int i, product = 1;
	for (i = 0; i < numOfIntegers; i++) product = product * arr[i];
	return product;
}

/*!
 * Find the average of the integers.
 *
 * @param arr[] the array that stores the integers.
 * @param numOfIntegers the number of integers.
 * @return average of the integers.
 */
float findAvg(int arr[], int size) { 
	float average;
	average = (float)findSum(arr, size) / size;
	return average;
}

/*!
 * Find the smallest integer.
 *
 * @param arr[] the array of the numbers are used.
 * @param size the size of the array.
 * @return the smallest element of the array.
 */
int findSmallest(int arr[], int size) { 
	int i, smallest = arr[0];
	for (i = 1; i < size; i++) {
		if (arr[i] < smallest) smallest = arr[i];
	}
	return smallest;
}

int main() {

	int num = 0, i = 0;
	string tempStr; // A temporary string to get string lines
	ifstream file("input.txt");

	if (file.is_open()) {
		getline(file, tempStr); // The first line
		while (i != tempStr.length()) { // To detect the count of integers from the first line
			if (isalnum(tempStr[i])) {
				num = num * 10 + tempStr[i] - 48; // 48 = Decimal value of '0' character
			}
			i++;
		}
		getline(file, tempStr); // The second and last line
		file.close();
	}
	else {
		cout << "Error : This file does not exist.\n";
		exit (0); // To stop the program
	}
	
	int* nums = new int[num + 1]; // The array that stores the integer. +1 is to detect wrong number of integers
	int sum, product, smallest;
	float avg;

	if (getNumbers(tempStr, nums, num)) {
		sum = findSum(nums, num);
		cout << "Sum is " << sum << endl;

		product = findProduct(nums, num);
		cout << "Product is " << product << endl;

		avg = findAvg(nums, num);
		cout << "Average is " << avg << endl;

		smallest = findSmallest(nums, num);
		cout << "Smallest is " << smallest;
	}
}