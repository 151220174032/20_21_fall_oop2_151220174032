#include <stdio.h>

void update(int* a, int* b) {
    int sum = *a + *b, subtract = *a - *b;
    *a = sum;
    if (subtract < 0) *b = -1 * subtract;
    if (subtract > 0) *b = subtract;
}

int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    scanf("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}
