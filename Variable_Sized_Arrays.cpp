#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int arrCount, queryCount, row = 0, column = 0;
    cin >> arrCount >> queryCount;
    int* arr[arrCount];

    while (arrCount--) {
        cin >> column;
        arr[row] = new int[column];
        for (int i = 0; i < column; i++) cin >> arr[row][i];
        row++;
    } // end-while
    while (queryCount--) {
        cin >> row >> column;
        cout << arr[row][column] << endl;
    } // end-while

    return 0;
}
