#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
#include <sstream>
#include <string>
using namespace std;


int main() {
    int N, Q;
    string current = "", att_name;
    map<string, string> k;
    cin >> N >> Q;
    cin.ignore();

    for (int i = 0; i < N; i++) {
        string line, Tag, temp;
        getline(cin, line);
        stringstream ss(line);

        while (getline(ss, temp, ' ')) {
            if (temp[0] == '<') {
                if (temp[1] != '/') {
                    Tag = temp.substr(1);
                    if (Tag[Tag.length() - 1] == '>') {
                        Tag.pop_back();
                    }
                    if (current.size() > 0) {
                        current += "." + Tag;
                    }
                    else {
                        current = Tag;
                    }
                }
                else {
                    Tag = temp.substr(2, (temp.find('>') - 2));
                    size_t pos = current.find("." + Tag);
                    if (pos != string::npos) {
                        current = current.substr(0, pos);
                    }
                    else {
                        current = "";
                    }
                }
            }

            else if (temp[0] == '"') {
                size_t pos = temp.find_last_of('"');
                string attr_value = temp.substr(1, pos - 1);
                k[att_name] = attr_value;
            }

            else {
                if (temp != "=") {
                    att_name = current + "~" + temp;
                }
            }
        }
    }

    string Queries;
    for (int i = 0; i < Q; i++) {
        getline(cin, Queries);

        map<string, string>::iterator itr = k.find(Queries);
        if (itr != k.end()) {
            cout << itr->second << endl;
        }
        else {
            cout << "Not Found!" << endl;
        }
    }

    return 0;
}