#include <iostream>
#include <string>
using namespace std;

int main() {
    string a, b, sum;
    char hold;

    cin >> a >> b;
    cout << a.size() << " " << b.size() << endl;

    sum = a + b;
    cout << sum << endl;

    hold = a[0];
    a[0] = b[0];
    b[0] = hold;
    cout << a << " " << b;

    return 0;
}
